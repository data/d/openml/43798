# OpenML dataset: Google-Play-Store-Apps

https://www.openml.org/d/43798

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Google PlayStore App analytics. (1.1 Million + App Data)
Source: https://github.com/gauthamp10/Google-Playstore-Dataset
Content
I've collected the data with the help of Python and Scrapy running on a google cloud vm instance.
The data was collected on December 2020.
Acknowledgements
I couldn't have build this dataset without the help of Google's Cloud engine.
Inspiration
Took inspiration from: https://www.kaggle.com/lava18/google-play-store-apps to build a big database for students and researchers.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43798) of an [OpenML dataset](https://www.openml.org/d/43798). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43798/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43798/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43798/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

